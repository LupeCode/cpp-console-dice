#include <iostream>
#include <random>
#include <sstream>
#include <regex>
#include "dice.h"

#ifndef ull
#define ull unsigned long long
#endif

using std::string;
using std::ostringstream;
using std::vector;
using std::cout;
using std::cin;
using std::endl;
using std::regex;
using std::smatch;
using std::exception;
using LupeCode::dice;

template<typename T>
string join(const T &v, const string &delim) {
    ostringstream s;
    for (const auto &i : v) {
        if (&i != &v[0])
            s << delim;
        s << i;
    }
    return s.str();
}

int main() {
    long long modifier = 0;
    string line, last_input = "q";
    dice die;

    cout << "Dice Roller v1.0" << endl;
    while (true) {
        cout << "================================================" << endl;
        cout << "Enter a dice text like 2d10 or 3d6+1, or q to exit: ";
        cout << "[" << last_input << "] ";
        getline(cin, line);
        if (line.length() == 0) line = last_input;
        last_input = line;

        if (die.parse_input(line)) {
            vector<ull> dice_rolled = die.roll(die.number, die.sides);
            cout << "The dice rolled: " << join(dice_rolled, ",") << endl << "That add up to " << die.last_sum();
            if (die.has_modifier)
                cout << ", " << die.last_sum() + modifier << " with modifier";
            cout << endl;
        } else {
            break;
        }

    }
    cout << "Thank you for rolling." << endl;
    return 0;
}
