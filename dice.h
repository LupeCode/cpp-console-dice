#ifndef DICE_DICE_H
#define DICE_DICE_H

#ifndef ull
#define ull unsigned long long
#endif

#ifndef sll
#define sll signed long long
#endif

#include <random>
#include <iostream>
#include <string>
#include <regex>

using std::mt19937_64;
using std::vector;
using std::random_device;
using std::uniform_int_distribution;
using std::string;
using std::cout;
using std::endl;
using std::regex;
using std::smatch;
using std::exception;

namespace LupeCode {
    class dice {

    protected:

        mt19937_64 rnd;
        vector<ull> rolls;
        ull sum = 0;

    public:

        ull number = 0, sides = 0;
        string dice_string = "";
        bool has_modifier = false, bias_high = false, bias_low = false;
        sll modifier = 0;
        float factor = 2.0f;

        dice () {
            rnd.seed(random_device()());
        }

        ull roll_one (ull sides) {
            uniform_int_distribution<ull> dx(1, sides);
            return dx(rnd);
        }

        vector<ull> roll (ull number, ull sides) {
            sum = 0;
            rolls = vector<ull>(number);
            for (ull i = 0; i < number; i++) {
                sum += rolls[i] = roll_one(sides);
            }
            return rolls;
        }

        vector<ull> roll_high (ull number, ull sides, float factor = 2.0f) {
            sides = (ull) floor(pow(sides, factor));
            rolls = vector<ull>(number);
            for (ull i = 0; i < number; i++) {
                sum += rolls[i] = (ull) ceil(pow(roll_one(sides), 1 / factor));
            }
            return rolls;
        }

        vector<ull> roll_low (ull number, ull sides, float factor = 2.0f) {
            rolls = roll_high(number, sides, factor);
            for (ull i = 0; i < number; i++) {
                rolls[i] = sides + 1 - rolls[i];
            }
            return rolls;
        }

        ull last_sum () {
            return sum;
        }

        bool parse_input (const string &input) {
            try {
                bias_high = bias_low = has_modifier = false;
                regex re(R"((\d+)[dD](\d+)(\+|\-)?(\d+)?([lhLH])?([\d\.]+)?)");
                smatch match;
                if (regex_search(input, match, re) && match.size() > 1) {
                    number = stoul(match.str(1));
                    sides = stoul(match.str(2));
                    if (match.size() > 4 && match.str(4).length() > 0) {
                        has_modifier = true;
                        modifier = stol(match.str(4));
                        if (match.str(3) == "-") modifier *= -1;
                    }
                    if (match.size() > 6 && match.str(6).length() > 0) {
                        factor = stof(match.str(6));
                        if (match.str(5) == "h" || match.str(5) == "H") bias_high = true;
                        else if (match.str(5) == "l" || match.str(5) == "L") bias_low = true;
                    }
                } else {
                    return false;
                }
            } catch (const exception &e) {
                cout << "ERROR!" << endl << e.what() << endl;
                return false;
            }
            return true;
        }

    };
}

#endif //DICE_DICE_H
